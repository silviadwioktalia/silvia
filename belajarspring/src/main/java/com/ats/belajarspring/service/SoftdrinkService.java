package com.ats.belajarspring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ats.belajarspring.model.SoftdrinkModel;
import com.ats.belajarspring.repository.SoftdrinkRepository;

@Service
@Transactional
public class SoftdrinkService {

	@Autowired
	private SoftdrinkRepository softdrinkRepository;
	
	public void createSoftdrink(SoftdrinkModel softdrinkModel) {
		softdrinkRepository.save(softdrinkModel);
	}
	
	public List<SoftdrinkModel> readAsc() {
		return softdrinkRepository.querySelectAsc();
	}
	
}
