package com.ats.belajarspring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ats.belajarspring.model.PresidenModel;
import com.ats.belajarspring.repository.PresidenRepository;



@Service
@Transactional
public class PresidenService {

	@Autowired
	private PresidenRepository presidenRepository;
	
	public void create(PresidenModel presidenModel) {
		presidenRepository.save(presidenModel);
	}
	
	public List<PresidenModel> read(){
		return presidenRepository.findAll();
	}
	
	public List<PresidenModel> readWhere(){
		return presidenRepository.querySelectWhere();
	}
	
}
