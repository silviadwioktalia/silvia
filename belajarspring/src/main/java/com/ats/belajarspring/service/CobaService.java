package com.ats.belajarspring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ats.belajarspring.model.CobaModel;
import com.ats.belajarspring.repository.CobaRepository;

@Service
@Transactional
public class CobaService {

	@Autowired
	private CobaRepository cobaRepository;
	
	public void createCoba(CobaModel cobaModel) {
		cobaRepository.save(cobaModel);
	}
	
	public List<CobaModel> readCoba() {
		return cobaRepository.findAll();
	}
	
	public List<CobaModel> readCobaDesc() {
		return cobaRepository.querySelectDesc();
	}
	
	public List<CobaModel> readCari(String kataCari) {
		return cobaRepository.queryCari(kataCari);
	}
	
}
