package com.ats.belajarspring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.repository.ProdukRepository;

@Service
@Transactional
public class ProdukService {

	@Autowired
	private ProdukRepository produkRepository;
	
	public void create(ProdukModel produkModel) {
		produkRepository.save(produkModel);
	}
	
	public List<ProdukModel> read(){
		return produkRepository.findAll();
	}
	
	public List<ProdukModel> searchNama(String kataCari){
		return produkRepository.querySelectAllWhereNama(kataCari);
	}
	
	public List<ProdukModel> searchKode(String kataCari){
		return produkRepository.querySelectAllKode(kataCari);
	}
	
	public List<ProdukModel> hargaProdukAsc(){
		return produkRepository.hargaProdukUrutAsc();
	}

	public void update(ProdukModel produkModel) {
		produkRepository.save(produkModel);
	}
	
	public void delete(String kodeProduk) {
		produkRepository.deleteById(kodeProduk);
	}
	
	public List<ProdukModel> readHargaParameter(int hargaCari){
		return produkRepository.hargaLebihParameter(hargaCari);
	}
}
