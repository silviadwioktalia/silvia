package com.ats.belajarspring.service;

import java.util.ArrayList;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.repository.DosenRepository;

@Service
@Transactional
public class DosenService {

	@Autowired
	private DosenRepository dosenRepository;
	
	// modifier kosong/tdk namaMethod(TipeVariable namaVariabel)
	public void create(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
		// method save tdk ada di repository krn berasal dari JpaRepository
	}
	
	public List<DosenModel> read() {
		return dosenRepository.findAll();
	}
	
	public List<DosenModel> readOrderNama() {
		return dosenRepository.querySelectAllOrderNamaDesc();
	}
	
	public List<DosenModel> readOrderBy() {
		return dosenRepository.querySelectAllOrderUsiaDesc();
	}
	
	public List<DosenModel> readWhereNama(String kataKunci) {
		return dosenRepository.querySelectAllWhereNama(kataKunci);
	}
	
	public List<DosenModel> readWhereUsia(int kataKunci) {
		return dosenRepository.querySelectAllWhereUsiaEquals(kataKunci);
	}
	
	public List<DosenModel> readWhereUsiaKurang(int kataKunci) {
		return dosenRepository.querySelectAllWhereUsiaKurang(kataKunci);
	}
	
	public List<DosenModel> readWhereUsiaLebih(int kataKunci) {
		return dosenRepository.querySelectAllWhereUsiaLebih(kataKunci);
	}
	
	public List<DosenModel> readWhereUsiaKurangEquals(int kataKunci) {
		return dosenRepository.querySelectAllWhereUsiaKurangEquals(kataKunci);
	}
	
	public List<DosenModel> readWhereUsiaLebihEquals(int kataKunci) {
		return dosenRepository.querySelectAllWhereUsiaLebihEquals(kataKunci);
	}
	
	public List<DosenModel> readWhereUsiaNotEquals(int kataKunci) {
		return dosenRepository.querySelectAllWhereUsiaNotEquals(kataKunci);
	}
	
	public List<DosenModel> readWhereUsia2(int kataKunci, String operator) {
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		
		if (operator.equals("=")) {
			dosenModelList = dosenRepository.querySelectAllWhereUsiaEquals(kataKunci);
		} else if (operator.equals(">")) {
			dosenModelList = dosenRepository.querySelectAllWhereUsiaLebih(kataKunci);
		} else if (operator.equals("<")) {
			dosenModelList = dosenRepository.querySelectAllWhereUsiaKurang(kataKunci);
		}else if (operator.equals("<=")) {
			dosenModelList = dosenRepository.querySelectAllWhereUsiaKurangEquals(kataKunci);
		}else if (operator.equals(">=")) {
			dosenModelList = dosenRepository.querySelectAllWhereUsiaLebihEquals(kataKunci);
		}else{
			dosenModelList = dosenRepository.querySelectAllWhereUsiaNotEquals(kataKunci);
		}
		return dosenModelList;
	}
	
	public DosenModel readById(String namaDosen) {
		return dosenRepository.querySelectWhereNama(namaDosen);
	}
	
	public void delete(String namaDosen) {
		dosenRepository.deleteById(namaDosen);
	}
	
	public void update(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	

	 public List<DosenModel> readCari(int kataKunci, String operator) {
	 
	 List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
	 
	 if (operator.equals("=")) { dosenModelList =
	 dosenRepository.queryCariH(); } else if
	 (operator.equals(">")) { dosenModelList =
	 dosenRepository.queryCariSLebihDari(kataKunci); } else if
	 (operator.equals("<")) { dosenModelList =
	 dosenRepository.queryCariSKurangDari(kataKunci); }else if
	 (operator.equals("<=") && kataKunci <= 30) { dosenModelList =
	 dosenRepository.queryCariF(); }else if
	 (operator.equals(">=") && kataKunci >= 30) { dosenModelList =
	 dosenRepository.queryCariS(); }else{ dosenModelList =
	 dosenRepository.querySelectAllOrderUsiaAsc(); } return
	 dosenModelList; }
	 
	 public List<DosenModel> readCari2(int kataKunci, String operator, String huruf) {
			
			List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
			
			if (operator.equals("=")) {
				dosenModelList = dosenRepository.queryCariHuruf(kataKunci,huruf);
			} else if (operator.equals(">")) {
				dosenModelList = dosenRepository.queryCariHuruf2(kataKunci,huruf);
			} else if (operator.equals("<")) {
				dosenModelList = dosenRepository.queryCariHuruf1(kataKunci,huruf);
			}else if (operator.equals("<=")) {
				dosenModelList = dosenRepository.queryCariHuruf3(kataKunci,huruf);
			}else if (operator.equals(">=")) {
				dosenModelList = dosenRepository.queryCariHuruf4(kataKunci,huruf);
			}else{
				dosenModelList = dosenRepository.queryCariHuruf5(kataKunci,huruf);
			}
			return dosenModelList;
		}
	 
	 public List<DosenModel> readUsiaLess25() {
			return dosenRepository.queryUsiaLess25();
		}
}
