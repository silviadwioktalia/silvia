package com.ats.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.KarakterModel;
import com.ats.belajarspring.service.KarakterService;

@RestController
@RequestMapping("/api/karakterApi")
public class KarakterApi {

	@Autowired
	private KarakterService karakterService;
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody KarakterModel karakterModel) {
		karakterService.create(karakterModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", Boolean.TRUE);
		map.put("pesan", "Data berhasil ditambahkan");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	public List<KarakterModel> getApi(@RequestBody KarakterModel karakterModel) {
		List<KarakterModel> karakterModelList = new ArrayList<KarakterModel>();
		karakterModelList = karakterService.read();
		return karakterModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody KarakterModel karakterModel) {
		karakterService.update(karakterModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", Boolean.TRUE);
		map.put("pesan", "Data berhasil diubah");
		return map;
	}
	
	@DeleteMapping("/delete/{namaKarakter}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String namaKarakter) {
		karakterService.delete(namaKarakter);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", Boolean.TRUE);
		map.put("pesan", "Data dengan nama "+namaKarakter+" berhasil dihapus");
		return map;
	}
	
}
