package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.KarakterModel;
import com.ats.belajarspring.service.KarakterService;


@Controller
public class KarakterController {

	@Autowired
	private KarakterService karakterService;
	
	@RequestMapping(value="tambah_karakter")
	public String menuIsiDosen() {
		String html = "karakter/isi_karakter";
		return html;
	}
	
	@RequestMapping(value="hasil_satu_karakter")
	public String menuIsiKarakter(HttpServletRequest request, Model model) {
		
		String namaKarakter = request.getParameter("tambah_nama");
		int levelKarakter = Integer.valueOf(request.getParameter("tambah_level"));
		String statusKarakter = request.getParameter("tambah_status");
		
		KarakterModel karakterModel = new KarakterModel();
		karakterModel.setNama(namaKarakter);
		karakterModel.setLevel(levelKarakter);
		karakterModel.setStatus(statusKarakter);
		
		karakterService.create(karakterModel);
		
		model.addAttribute("karakterModel", karakterModel);
		
		String html = "karakter/satu_karakter";
		return html;
	}
	
	@RequestMapping(value="hasil_banyak_karakter")
	public String menuBanyakKarakter(Model model) {
		
		
		 List<KarakterModel> karakterModelList = new ArrayList<KarakterModel>();
		 karakterModelList = karakterService.read();
		 model.addAttribute("karakterModelList", karakterModelList);
		
		
		String html = "karakter/banyak_karakter";
		return html;
		
	}
	
	
	//cara lama tidak menyimpan ke database krn tidak ada method untuk insertnya
	// hanya menyimpan sementara di POJO
	@RequestMapping(value="hasil_satu_karakter_cara_lama")
	public String menuSatuKarakter(HttpServletRequest request, Model model) {
		
		// get data dari textfield di frontend isi_karakter.html
		String namaKarakter = request.getParameter("tambah_nama");
		int levelKarakter = Integer.valueOf(request.getParameter("tambah_level"));
		String statusKarakter = request.getParameter("tambah_status");
		
		// set data ke model
		KarakterModel karakterModel = new KarakterModel();
		karakterModel.setNama(namaKarakter);
		karakterModel.setLevel(levelKarakter);
		karakterModel.setStatus(statusKarakter);
		
		// untuk lempar data ke frontend satu_karakter.hml
		model.addAttribute("karakterModel", karakterModel);
		
		// mapping
		String html = "karakter/satu_karakter_cara_lama";
		return html;
	}

}
