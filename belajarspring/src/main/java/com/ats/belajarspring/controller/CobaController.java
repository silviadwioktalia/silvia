package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.CobaModel;
import com.ats.belajarspring.service.CobaService;

@Controller
public class CobaController {

	@Autowired
	private CobaService cobaService;
	
	@RequestMapping(value="menu_tambah_coba")
	public String menuTambahCoba() {
		
		String html = "coba/tambah_coba";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_coba")
	public String prosesTambahCoba(HttpServletRequest request, Model model) {
		String namaCoba = request.getParameter("tambah_nama");
		int jumlahCoba = Integer.valueOf(request.getParameter("tambah_jumlah"));
		
		CobaModel cobaModel = new CobaModel();
		cobaModel.setNamaCoba(namaCoba);
		cobaModel.setJumlahCoba(jumlahCoba);
		
		cobaService.createCoba(cobaModel);
		
		model.addAttribute("cobaModel", cobaModel);
		
		String html = "coba/hasil_coba";
		return html;
	}
	
	@RequestMapping(value="menu_daftar_coba")
	public String menuDaftarCoba(Model model) {
		
		List<CobaModel> cobaModelList = new ArrayList<CobaModel>();
		cobaModelList = cobaService.readCoba();
		model.addAttribute("cobaModelList", cobaModelList);
		
		String html = "coba/daftar_coba";
		return html;
	}
	
	@RequestMapping(value="menu_daftar_coba_desc")
	public String menuDaftarCobaAsc(Model model) {
		
		List<CobaModel> cobaModelList = new ArrayList<CobaModel>();
		cobaModelList = cobaService.readCobaDesc();
		model.addAttribute("cobaModelList", cobaModelList);
		
		String html = "coba/daftar_coba_desc";
		return html;
	}
	
	@RequestMapping(value="proses_cari")
	public String menuDaftarCobaCari(HttpServletRequest request, Model model) {
		String kataCari = request.getParameter("cari");
		
		List<CobaModel> cobaModelList = new ArrayList<CobaModel>();
		cobaModelList = cobaService.readCari(kataCari);
		model.addAttribute("cobaModelList", cobaModelList);
		
		String html = "coba/daftar_coba";
		return html;
	}
}
