package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.service.DosenService;


@Controller
public class DosenController {

	// sebagai pengganti instance = IOC / DI
	@Autowired
	private DosenService dosenService;
	
	@RequestMapping(value="tambah_dosen")
	public String menuIsiDosen() {
		String html = "dosen/isi_dosen";
		return html;
	}
	
	
	@RequestMapping(value="hasil_satu_dosen")
	public String menuSatuDosen(HttpServletRequest request, Model model) {
		
		String namaDosen = request.getParameter("tambah_nama_dosen");
		int usiaDosen = Integer.valueOf(request.getParameter("tambah_usia_dosen"));
				
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);

		// Transaksi simpan C create
		dosenService.create(dosenModel);
		// untuk memanggil method create yg ada di service
		// maka kita perlu menginstance DosenService, seperti
		// DosenService dosenService = new DosenService();
		// Agar tdk banyak menginstance class yg sama, maka digunakan autowired
		// sehingga method2 yg ada di service bisa langsung dipanggil saja
		
	
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/satu_dosen";
		return html;
	}
	
	@RequestMapping(value="hasil_banyak_dosen")
	public String menuBanyakDosen(Model model) {
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	@RequestMapping(value="hasil_banyak_dosen_order_nama")
	public String menuBanyakDosenOrderNama(Model model) {
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderNama();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen_order_nama";
		return html;
	}
	
	@RequestMapping(value="order_dosen")
	public String menuOrderDosen(Model model) {
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/order_dosen";
		return html;
	}
	
	@RequestMapping(value="cari_nama_dosen")
	public String menuCariNamaDosen(Model model) {
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	@RequestMapping(value="proses_cari_nama")
	public String menuProsesCariNamaDosen(HttpServletRequest request, Model model) {
		String kataKunci = request.getParameter("katakunci_nama");
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readWhereNama(kataKunci);
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	@RequestMapping(value="cari_usia_dosen")
	public String menuCariusiaDosen(Model model) {
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia";
		return html;
	}
	
	@RequestMapping(value="proses_cari_usia")
	public String menuProsesCariUsiaDosen(HttpServletRequest request, Model model) {
		int kataKunci = Integer.valueOf(request.getParameter("katakunci_usia"));
		
		System.out.println(kataKunci);
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readWhereUsia(kataKunci);
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia";
		return html;
	}
	
	@RequestMapping(value="cari_usia_beragam")
	public String menuCariUsiaBeragam(Model model) {
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia_beragam";
		return html;
	}
	
	@RequestMapping(value="proses_cari_usia_beragam")
	public String menuProsesCariUsiaBeragam(HttpServletRequest request, Model model) {
		int kataKunci = Integer.valueOf(request.getParameter("katakunci_usia"));
		String operator = request.getParameter("daftar_operator");
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();

		switch (operator) {
		case "=":
			dosenModelList = dosenService.readWhereUsia(kataKunci);
			break;
		case "<":
			dosenModelList = dosenService.readWhereUsiaKurang(kataKunci);
			break;
		case ">":
			dosenModelList = dosenService.readWhereUsiaLebih(kataKunci);
			break;
		case "<=":
			dosenModelList = dosenService.readWhereUsiaKurangEquals(kataKunci);
			break;
		case ">=":
			dosenModelList = dosenService.readWhereUsiaLebihEquals(kataKunci);
			break;
		default:
			dosenModelList = dosenService.readWhereUsiaNotEquals(kataKunci);
			break;
		}
		
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia_beragam";
		return html;
	}
	
	@RequestMapping(value="cari_usia_beragam2")
	public String menuCariUsiaBeragam2(Model model) {
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia_beragam2";
		return html;
	}
	
	@RequestMapping(value="proses_cari_usia_beragam2")
	public String menuProsesCariUsiaBeragam2(HttpServletRequest request, Model model) {
		int kataKunci = Integer.valueOf(request.getParameter("katakunci_usia"));
		String operator = request.getParameter("daftar_operator");
		String huruf = request.getParameter("daftar_huruf");
		
		// bagian list data
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readCari2(kataKunci,operator,huruf);
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia_beragam2";
		return html;
	}
	
	@RequestMapping(value="ubah_dosen")
	public String menuUbahDosen(HttpServletRequest request, Model model) {
		
		String namaId = request.getParameter("namaId");
		
		DosenModel dosenModel = new DosenModel();
		dosenModel = dosenService.readById(namaId);
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/ubah_dosen";
		return html;
	}
	
	@RequestMapping(value="proses_ubah_dosen")
	public String menuProsesUbahDosen(HttpServletRequest request, Model model) {
		
		String namaId = request.getParameter("namaId");
		int usiaDosen = Integer.valueOf(request.getParameter("ubah_usia_dosen"));
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaId);
		dosenModel.setUsiaDosen(usiaDosen);
		
		dosenService.update(dosenModel);	
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen";
		return html;
		
	}
	
	@RequestMapping(value="hapus_dosen")
	public String menuHapusDosen(HttpServletRequest request, Model model) {
		String namaId = request.getParameter("namaId");
		DosenModel dosenModel = new DosenModel();
		dosenModel = dosenService.readById(namaId);
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/hapus_dosen";
		return html;
	}
	
	@RequestMapping(value="proses_hapus_dosen")
	public String menuProsesHapusDosen(HttpServletRequest request, Model model) {
		
		String namaId = request.getParameter("namaId");
		dosenService.delete(namaId);
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen";
		return html;
		
	}
	
}
