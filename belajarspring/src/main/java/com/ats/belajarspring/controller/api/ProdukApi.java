package com.ats.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.service.ProdukService;

@RestController
@RequestMapping("/api/produk")
public class ProdukApi {

	@Autowired
	private ProdukService produkService;
	
	@PostMapping("/post")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Map<String, Object> postProduk(@RequestBody ProdukModel produkModel) {
		produkService.create(produkModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", Boolean.TRUE);
		map.put("pesan", "Data berhasil ditambahkan");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code=HttpStatus.OK)
	public List<ProdukModel> getProduk() {
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList=produkService.read();
		return produkModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code=HttpStatus.OK)
	public void putProduk(@RequestBody ProdukModel produkModel) {
		produkService.update(produkModel);
	}
	
	@DeleteMapping("/delete/{kodeProduk}")
	@ResponseStatus(code=HttpStatus.GONE)
	public void deleteProduk(@PathVariable String kodeProduk) {
		produkService.delete(kodeProduk);
	}
	
	@GetMapping("/getnama/{kataCari}")
	@ResponseStatus(code=HttpStatus.OK)
	public List<ProdukModel> getNamaProduk(@PathVariable String kataCari) {
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList=produkService.searchNama(kataCari);
		return produkModelList;
	}
	
	
	@GetMapping("/gethargalebih/{hargaCari}")
	@ResponseStatus(code=HttpStatus.OK)
	public List<ProdukModel> getHargaProdukLebih(@PathVariable int hargaCari) {
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList=produkService.readHargaParameter(hargaCari);
		return produkModelList;
	}
}
