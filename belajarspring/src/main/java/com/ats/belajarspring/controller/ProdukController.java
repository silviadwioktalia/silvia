package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.service.ProdukService;

@Controller
public class ProdukController {

	@Autowired
	private ProdukService produkService;
	
	@RequestMapping(value = "menu_tambah_produk")
	public String menuTambahProduk() {
		
		String html="produk/tambah_produk";
		return html;
		
	}
	
	@RequestMapping(value = "proses_tambah_produk")
	public String prosesTambahProduk(HttpServletRequest request, Model model) {
		
		String kodeProduk = request.getParameter("tambah_kode");
		String namaProduk = request.getParameter("tambah_nama");
		int hargaProduk = Integer.valueOf(request.getParameter("tambah_harga"));
		
		ProdukModel produkModel = new ProdukModel();
		produkModel.setKodeProduk(kodeProduk);
		produkModel.setNamaProduk(namaProduk);
		produkModel.setHargaProduk(hargaProduk);
		
		produkService.create(produkModel);
		
		model.addAttribute("produkModel", produkModel);
		
		String html="produk/sukses";
		return html;
	}
	
	@RequestMapping(value = "menu_daftar_produk")
	public String menuDaftarProduk(Model model) {
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.read();
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/daftar_produk";
		return html;
	}
	
	@RequestMapping(value = "proses_cari_produk")
	public String menuCariNama(HttpServletRequest request, Model model) {
		String kataCari = request.getParameter("cari_nama");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.searchNama(kataCari);
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/daftar_produk";
		return html;
	}
	
	@RequestMapping(value = "proses_cari_kode")
	public String menuCari2(HttpServletRequest request, Model model) {
		String kataCari = request.getParameter("cari_kode");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.searchKode(kataCari);
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/daftar_produk";
		return html;
	}
	
	@RequestMapping(value = "menu_urut_asc")
	public String menuUrutAsc(Model model) {
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.hargaProdukAsc();
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/urut_asc";
		return html;
	}
	
}
