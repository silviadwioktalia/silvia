package com.ats.belajarspring.controller.api;

import java.util.ArrayList;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.service.DosenService;

@RestController
@RequestMapping(value="api/dosenApi")
public class DosenApi {

	@Autowired
	private DosenService dosenService;
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody DosenModel dosenModel){ //Map<K, V> k itu key/tipe data dan v itu value/object
		this.dosenService.create(dosenModel);
		
		//Induknya Map dibawahnya ada Hash Map
		//Hash map implementasi dari Map
		//Map digunakan untuk buat notifikasi biar muncul di postman
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil dimasukkan");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DosenModel> getApi(){
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.read();
		return dosenModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody DosenModel dosenModel){ 
		this.dosenService.update(dosenModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil dimasukkan");
		return map;
	}
	
	@DeleteMapping("/delete/{namaDosen}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String namaDosen){
		this.dosenService.delete(namaDosen);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE); 
		map.put("pesan", "selamat data anda dengan nama "+namaDosen+" berhasil dihapus");
		return map;
	}

	@GetMapping("/getusialessthan")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DosenModel> getApiLess(){
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readUsiaLess25();
		return dosenModelList;
	}
	
	@GetMapping("/getusiaparameter/{kataKunci}")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DosenModel> getApiUsiaParameter(@PathVariable int kataKunci){
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = dosenService.readWhereUsia(kataKunci);
		return dosenModelList;
	}
	
}
