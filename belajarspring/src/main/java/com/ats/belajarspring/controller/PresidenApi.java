package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.PresidenModel;
import com.ats.belajarspring.service.PresidenService;

@RestController
@RequestMapping("/api/presiden")
public class PresidenApi {

	@Autowired
	private PresidenService presidenService;
	
	@PostMapping("/post")
	@ResponseStatus(code=HttpStatus.CREATED)
	public void postPresiden(@RequestBody PresidenModel presidenModel) {
		presidenService.create(presidenModel);
			}
	
	@GetMapping("/get")
	@ResponseStatus(code=HttpStatus.OK)
	public List<PresidenModel> getPresiden() {
		List<PresidenModel> presidenModelList = new ArrayList<PresidenModel>();
		presidenModelList=presidenService.read();
		return presidenModelList;
	}
	
	@GetMapping("/getpresiden")
	@ResponseStatus(code=HttpStatus.OK)
	public List<PresidenModel> getPresidenWhere() {
		List<PresidenModel> presidenModelList = new ArrayList<PresidenModel>();
		presidenModelList=presidenService.readWhere();
		return presidenModelList;
	}
	
}
