package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.SoftdrinkModel;
import com.ats.belajarspring.service.SoftdrinkService;

@Controller
public class SoftdrinkController {

	@Autowired
	private SoftdrinkService softdrinkService;
	
	@RequestMapping(value="menu_tambah_softdrink")
	public String menuTambahSoftdrink() {
		String html="softdrink/tambah_softdrink";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_softdrink")
	public String prosesTambahSoftdrink(HttpServletRequest request, Model model) {
		String kodeSoftdrink = request.getParameter("tambah_kode");
		String namaSoftdrink = request.getParameter("tambah_nama");
		int kaloriSoftdrink = Integer.valueOf(request.getParameter("tambah_kalori"));
		
		SoftdrinkModel softdrinkModel = new SoftdrinkModel();
		softdrinkModel.setKodeSoftdrink(kodeSoftdrink);
		softdrinkModel.setNamaSoftdrink(namaSoftdrink);
		softdrinkModel.setKaloriSoftdrink(kaloriSoftdrink);
		
		softdrinkService.createSoftdrink(softdrinkModel);
		
		List<SoftdrinkModel> softdrinkModelList = new ArrayList<SoftdrinkModel>();
		softdrinkModelList = softdrinkService.readAsc();
		model.addAttribute("softdrinkModelList", softdrinkModelList);
		
		String html="softdrink/daftar_softdrink";
		return html;
	}
}
