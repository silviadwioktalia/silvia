package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_PRESIDEN")
public class PresidenModel {

	@Id
	@Column(name="NO_PRESIDEN")
	private int noPresiden;
	
	@Column(name="NAMA_PRESIDEN")
	private String namaPresiden;
	
	@Column(name="KOTA_PRESIDEN")
	private String kotaPresiden;
	
	@Column(name="USIA_PRESIDEN")
	private int usiaPresiden;
	
	public int getNoPresiden() {
		return noPresiden;
	}
	public void setNoPresiden(int noPresiden) {
		this.noPresiden = noPresiden;
	}
	public String getNamaPresiden() {
		return namaPresiden;
	}
	public void setNamaPresiden(String namaPresiden) {
		this.namaPresiden = namaPresiden;
	}
	public String getKotaPresiden() {
		return kotaPresiden;
	}
	public void setKotaPresiden(String kotaPresiden) {
		this.kotaPresiden = kotaPresiden;
	}
	public int getUsiaPresiden() {
		return usiaPresiden;
	}
	public void setUsiaPresiden(int usiaPresiden) {
		this.usiaPresiden = usiaPresiden;
	}
	
	
	
}
