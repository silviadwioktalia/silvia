package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_SOFTDRINK")
public class SoftdrinkModel {

	@Id
	@Column(name="KD_SOFDRINK")
	private String kodeSoftdrink;
	
	@Column(name="NM_SOFDRINK")
	private String namaSoftdrink;
	
	@Column(name="KLR_SOFDRINK")
	private int kaloriSoftdrink;
	
	
	public String getKodeSoftdrink() {
		return kodeSoftdrink;
	}
	public void setKodeSoftdrink(String kodeSoftdrink) {
		this.kodeSoftdrink = kodeSoftdrink;
	}
	public String getNamaSoftdrink() {
		return namaSoftdrink;
	}
	public void setNamaSoftdrink(String namaSoftdrink) {
		this.namaSoftdrink = namaSoftdrink;
	}
	public int getKaloriSoftdrink() {
		return kaloriSoftdrink;
	}
	public void setKaloriSoftdrink(int kaloriSoftdrink) {
		this.kaloriSoftdrink = kaloriSoftdrink;
	}
	
	
}
