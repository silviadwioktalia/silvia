package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="T_COBA")
public class CobaModel {

	@Id
	@Column(name="NM_COBA")
	private String namaCoba;
	
	@Column(name="JML_COBA")
	private int jumlahCoba;
	
	public String getNamaCoba() {
		return namaCoba;
	}
	public void setNamaCoba(String namaCoba) {
		this.namaCoba = namaCoba;
	}
	public int getJumlahCoba() {
		return jumlahCoba;
	}
	public void setJumlahCoba(int jumlahCoba) {
		this.jumlahCoba = jumlahCoba;
	}

}
