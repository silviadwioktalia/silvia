package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.ProdukModel;

public interface ProdukRepository extends JpaRepository<ProdukModel, String>{

	@Query("SELECT P FROM ProdukModel P WHERE P.namaProduk LIKE %?1%")
	List<ProdukModel> querySelectAllWhereNama(String kataCari);
	
	@Query("SELECT P FROM ProdukModel P WHERE P.kodeProduk LIKE %?1%")
	List<ProdukModel> querySelectAllKode(String kataCari);
	
	@Query("SELECT P FROM ProdukModel P ORDER BY P.hargaProduk ASC")
	List<ProdukModel> hargaProdukUrutAsc();
	
	@Query("SELECT P FROM ProdukModel P WHERE P.hargaProduk > ?1")
	List<ProdukModel> hargaLebihParameter(int hargaCari);
}
