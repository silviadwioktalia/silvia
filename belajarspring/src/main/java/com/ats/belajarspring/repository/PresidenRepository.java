package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.PresidenModel;

public interface PresidenRepository extends JpaRepository<PresidenModel, Integer>{

	@Query("SELECT P FROM PresidenModel P WHERE P.namaPresiden LIKE '%O%' OR P.usiaPresiden = 70")
	List<PresidenModel> querySelectWhere();
	
}
