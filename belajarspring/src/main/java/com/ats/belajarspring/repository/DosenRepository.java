package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.DosenModel;

public interface DosenRepository extends JpaRepository<DosenModel, String>{

	// Ini bukan SQL tapi HQL (Hibernate Query Language)
	// HQL itu query yg bisa dijalankan pada hibernate
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen DESC")
	List<DosenModel> querySelectAllOrderNamaDesc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen ASC")
	List<DosenModel> querySelectAllOrderNamaAsc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen DESC")
	List<DosenModel> querySelectAllOrderUsiaDesc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen ASC")
	List<DosenModel> querySelectAllOrderUsiaAsc();
	
	// karena nama berupa string maka itu adalah suatu kondisi yg tidak pasti
	// artinya where disini harus menggunakan like
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1%")
	List<DosenModel> querySelectAllWhereNama(String kataKunci);
	
	// karena usia berupa angka maka itu adalah suatu kondisi yg pasti
	// artinya where disini harus menggunaka operator aritmatika
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen = ?1")
	List<DosenModel> querySelectAllWhereUsiaEquals(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < ?1")
	List<DosenModel> querySelectAllWhereUsiaKurang(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen > ?1")
	List<DosenModel> querySelectAllWhereUsiaLebih(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen <= ?1")
	List<DosenModel> querySelectAllWhereUsiaKurangEquals(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen >= ?1")
	List<DosenModel> querySelectAllWhereUsiaLebihEquals(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen != ?1")
	List<DosenModel> querySelectAllWhereUsiaNotEquals(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen = ?1")
	DosenModel querySelectWhereNama(String namaDosen);
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE 'S%'")
	 List<DosenModel> queryCariS();
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE 'F%'")
	 List<DosenModel> queryCariF();
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE 'G%'")
	 List<DosenModel> queryCariG();
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE 'H%'")
	 List<DosenModel> queryCariH();
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen > ?1 AND D.namaDosen LIKE 'S%'")
	 List<DosenModel> queryCariSLebihDari(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < ?1 AND D.namaDosen LIKE 'S%'")
	 List<DosenModel> queryCariSKurangDari(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < ?1 AND D.namaDosen LIKE 'S%'")
	 List<DosenModel> queryCari2S(int kataKunci);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen = ?1 AND D.namaDosen LIKE ?2%")
	 List<DosenModel> queryCariHuruf(int kataKunci, String huruf);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < ?1 AND D.namaDosen LIKE ?2%")
	 List<DosenModel> queryCariHuruf1(int kataKunci, String huruf);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen > ?1 AND D.namaDosen LIKE ?2%")
	 List<DosenModel> queryCariHuruf2(int kataKunci, String huruf);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen <= ?1 AND D.namaDosen LIKE ?2%")
	 List<DosenModel> queryCariHuruf3(int kataKunci, String huruf);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen >= ?1 AND D.namaDosen LIKE ?2%")
	 List<DosenModel> queryCariHuruf4(int kataKunci, String huruf);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen != ?1 AND D.namaDosen LIKE ?2%")
	 List<DosenModel> queryCariHuruf5(int kataKunci, String huruf);

	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < 25")
	List<DosenModel> queryUsiaLess25();
}
