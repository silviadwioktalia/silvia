package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.CobaModel;

public interface CobaRepository extends JpaRepository<CobaModel, String>{

	@Query("SELECT C FROM CobaModel C ORDER BY C.jumlahCoba DESC")
	List<CobaModel> querySelectDesc();
	
	@Query("SELECT C FROM CobaModel C WHERE C.namaCoba LIKE %?1%")
	List<CobaModel> queryCari(String kataCari);
}
