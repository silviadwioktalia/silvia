package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.SoftdrinkModel;

public interface SoftdrinkRepository extends JpaRepository<SoftdrinkModel, String>{

	@Query("SELECT S FROM SoftdrinkModel S ORDER BY S.kaloriSoftdrink ASC")
	List<SoftdrinkModel> querySelectAsc();
	
}
