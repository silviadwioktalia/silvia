package com.example.cinema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.cinema.model.NegaraModel;

public interface NegaraRepository extends JpaRepository<NegaraModel, String> {

	@Query("SELECT N FROM NegaraModel N WHERE N.kodeNegara = ?1")
	NegaraModel searchKodeNegara (String kodeNegara);
	
	@Query("SELECT N FROM NegaraModel N WHERE N.namaNegara LIKE %?1%")
	List<NegaraModel> searchNamaNegara(String namaNegara);
	
}
