package com.example.cinema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.cinema.model.FilmModel;

public interface FilmRepository extends JpaRepository<FilmModel, String>{

	@Query("select f from FilmModel f WHERE f.kodeFilm like %?1%")
	List<FilmModel> searchKodeFilm (String kodeFilm);
	
	@Query("select f from FilmModel f WHERE f.kodeFilm = ?1")
	FilmModel search(String kodeFilm);
	
	@Query("select f from FilmModel f WHERE f.namaFilm like %?1%")
	List<FilmModel> name(String namaFilm);
}
