package com.example.cinema.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.cinema.model.NegaraModel;
import com.example.cinema.service.NegaraService;

@Controller
@RequestMapping("/negara")
public class NegaraController {

	@Autowired
	private NegaraService negaraService;
	
		//url ke halaman home.html
		@RequestMapping("/home") 
		public String doHome() {
			String page = "/negara/home";
			return page;
		}
		
		//url ke halaman add.html
		@RequestMapping("/tambah") 
		public String doAdd() {
			String page = "/negara/add";
			return page;
		}
		
		//url ke halaman list.html
		@RequestMapping(value="/data")
		public String list(Model model) {
			this.negaraRead(model);
			
			String list = "/negara/list";
			return list;
		}
		
		// tampilkan semua data/list data yang ada ditable
		public void negaraRead(Model model) {
			List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
			negaraModelList = negaraService.read();
			model.addAttribute("negaraModelList", negaraModelList);
		}
		
		// create atau insert data ke database
		@RequestMapping("/create")
		public String doCreate(HttpServletRequest request, Model model) {
			String kodeNegara = request.getParameter("kodeNegara");
			String namaNegara = request.getParameter("namaNegara");
			System.out.println(kodeNegara);
			System.out.println(namaNegara);
			
			NegaraModel negaraModel = new NegaraModel();
			negaraModel.setKodeNegara(kodeNegara);
			negaraModel.setNamaNegara(namaNegara);
			
			this.negaraService.create(negaraModel);
			this.negaraRead(model);
			
			String page = "/negara/home";
			return page;
		}
		
		//lihat detail data
				@RequestMapping(value="/detail")
				public String doDetail(HttpServletRequest request, Model model) {
					
					String kodeNegara = request.getParameter("kodeNegara");
					System.out.println(kodeNegara);
					
					NegaraModel negaraModel = new NegaraModel();
					negaraModel = this.negaraService.searchKodeNegara(kodeNegara);
					model.addAttribute("negaraModel", negaraModel); 
					
					String page = "/negara/detail";
					return page;
				}
				
				// untuk button ubah agar ke ubah.html dan menampilkan data di text field yg ada di form ubah data
				@RequestMapping("/ubah")
				public String doUbah(HttpServletRequest request, Model model) {
					String kodeNegara = request.getParameter("kodeNegara");
							
					NegaraModel negaraModel = new NegaraModel();
					negaraModel = this.negaraService.searchKodeNegara(kodeNegara);
					model.addAttribute("negaraModel", negaraModel);
							
					String page = "/negara/ubah";
				return page;
				}
						
						
				// update atau ubah data ke database
				@RequestMapping("/update")
				public String doUpdate(HttpServletRequest request, Model model) {
					String kodeNegara = request.getParameter("kodeNegara");
					String namaNegara = request.getParameter("namaNegara");
					System.out.println(kodeNegara);
					System.out.println(namaNegara);
							
					NegaraModel negaraModel = new NegaraModel();
					negaraModel.setKodeNegara(kodeNegara);
					negaraModel.setNamaNegara(namaNegara);
							
					this.negaraService.update(negaraModel);
					this.negaraRead(model);
							
					String page = "/negara/home";
					return page;
				}
				
				//search nama artis
				@RequestMapping(value="/search")
				public String cariNamaNegara(HttpServletRequest request, Model model) {
							
					String namaNegara = request.getParameter("namaNegara");
							
					List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
					negaraModelList = negaraService.searchNamaNegara(namaNegara);
					model.addAttribute("negaraModelList", negaraModelList);
					String html = "/negara/search";
					return html;
				}
				
				//url ke hapus.html
				@RequestMapping("/hapus") //url ke halaman hapus.html
				public String doHapus(HttpServletRequest request, Model model) {
					String kodeNegara = request.getParameter("kodeNegara");
					
					NegaraModel negaraModel = new NegaraModel();
					negaraModel = this.negaraService.searchKodeNegara(kodeNegara);
					model.addAttribute("negaraModel", negaraModel);
					String page = "/negara/hapus";
					return page;
				}
				
				
				// method agar data bisa terhapus
				@RequestMapping("/delete")
				public String doDelete(HttpServletRequest request, Model model) {
					String kodeNegara = request.getParameter("kodeNegara");
									
					NegaraModel negaraModel = new NegaraModel();
					negaraModel = this.negaraService.searchKodeNegara(kodeNegara);
									
					this.negaraService.delete(negaraModel);
									
					String page = "/negara/home";
					return page;
				}
		
}
