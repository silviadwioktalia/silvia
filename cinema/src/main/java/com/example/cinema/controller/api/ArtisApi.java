package com.example.cinema.controller.api;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.cinema.model.ArtisModel;
import com.example.cinema.repository.ArtisRepository;
import com.example.cinema.service.ArtisService;

@RestController
@RequestMapping("/api/artisApi")
public class ArtisApi {

	@Autowired
	private ArtisService artisService;
	
	@Autowired
	private ArtisRepository artisRepository;
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED) 
	public Map<String, Object> postApi(@RequestBody ArtisModel artisModel){ //Map<K, V> k itu key/tipe data v itu value/object
		this.artisService.create(artisModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		//Induknya Map dibawahnya ada Has Map
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil dimasukkan");
		return map;
	}
	// http://localhost:8282/cinema/api/artisApi/post
	// masukkan data yang ingin di insert
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	public List<ArtisModel> getApi(){
		List<ArtisModel> artisModelList = new ArrayList<ArtisModel>();
		artisModelList = this.artisService.read();
		return artisModelList;
	}
	// http://localhost:8282/cinema/api/artisApi/get
	// nanti semua data akan tampil
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody ArtisModel artisModel){
		this.artisService.update(artisModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil diubah");
		return map;
	}
	// cara panggil : http://localhost:8282/cinema/api/artisApi/put
	// dan masukkan data yang akan diubah
	
	@DeleteMapping("/delete/{kodeArtis}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String kodeArtis){
		this.artisService.delete(artisRepository.searchKodeArtis(kodeArtis));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE); 
		map.put("pesan", "selamat data anda dengan kode "+kodeArtis+" berhasil dihapus");
		return map;
	}
	// cara panggil : http://localhost:8282/cinema/api/artisApi/delete/kodeArtis
}
