package com.example.cinema.controller.api;

import java.util.ArrayList;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.cinema.model.FilmModel;
import com.example.cinema.repository.FilmRepository;
import com.example.cinema.service.FilmService;

@RestController
@RequestMapping("/api/filmApi")
public class FilmApi {

	@Autowired
	private FilmService filmService;
	
	@Autowired
	private FilmRepository filmRepository;
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED) 
	public Map<String, Object> postApi(@RequestBody FilmModel filmModel){ //Map<K, V> k itu key/tipe data v itu value/object
		this.filmService.create(filmModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		//Induknya Map dibawahnya ada Has Map
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil dimasukkan");
		return map;
	}


	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	public List<FilmModel> getApi(){
		List<FilmModel> filmModelList = new ArrayList<FilmModel>();
		filmModelList = this.filmService.read();
		return filmModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody FilmModel filmModel){
		this.filmService.update(filmModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil diubah");
		return map;
	}
	
	@DeleteMapping("/delete/{kodeFilm}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String kodeFilm){
		this.filmService.delete(filmRepository.search(kodeFilm));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE); 
		map.put("pesan", "selamat data anda dengan kode "+kodeFilm+" berhasil dihapus");
		return map;
	}
}
