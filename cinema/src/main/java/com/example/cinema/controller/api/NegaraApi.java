package com.example.cinema.controller.api;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.cinema.model.NegaraModel;
import com.example.cinema.repository.NegaraRepository;
import com.example.cinema.service.NegaraService;

@RestController
@RequestMapping("/api/negaraApi")
public class NegaraApi {

	@Autowired
	private NegaraService negaraService;
	
	@Autowired
	private NegaraRepository negaraRepository;
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED) 
	public Map<String, Object> postApi(@RequestBody NegaraModel negaraModel){ //Map<K, V> k itu key/tipe data v itu value/object
		this.negaraService.create(negaraModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		//Induknya Map dibawahnya ada Has Map
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil dimasukkan");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	public List<NegaraModel> getApi(){
		List<NegaraModel> negaraModelList = new ArrayList<NegaraModel>();
		negaraModelList = this.negaraService.read();
		return negaraModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody NegaraModel negaraModel){
		this.negaraService.update(negaraModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "selamat data berhasil diubah");
		return map;
	}
	
	@DeleteMapping("/delete/{kodeNegara}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String kodeNegara){
		this.negaraService.delete(negaraRepository.searchKodeNegara(kodeNegara));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE); 
		map.put("pesan", "selamat data anda dengan kode "+kodeNegara+" berhasil dihapus");
		return map;
	}
	
}
