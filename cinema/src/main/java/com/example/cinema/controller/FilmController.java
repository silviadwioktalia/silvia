package com.example.cinema.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.cinema.model.ArtisModel;
import com.example.cinema.model.FilmModel;
import com.example.cinema.service.ArtisService;
import com.example.cinema.service.FilmService;


@Controller
@RequestMapping("/film")
public class FilmController {

	@Autowired
	FilmService filmService;
	
	@Autowired
	ArtisService artisService;
	
	@RequestMapping("/home")
	public String doHome() {
		String home = "/film/home";
		return home;
	}
	
	@RequestMapping("/add")
	public String doAdd(Model model) {
		this.doListArtis(model);
		String add = "/film/add";
		return add;
	}
	
	@RequestMapping("/artis")
	public void doListArtis(Model model) {
		List<ArtisModel> artisModelList = new ArrayList<>();
		artisModelList = this.artisService.read();
		model.addAttribute("artisModelList", artisModelList);

	}
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request, Model model) {
		String kodeFilm = request.getParameter("kodeFilm");
		String namaFilm = request.getParameter("namaFilm");
		String genreFilm = request.getParameter("genreFilm");
		String kodeArtis = request.getParameter("artis");
		String produserFilm = request.getParameter("produserFilm");
		int pendapatanFilm = Integer.parseInt(request.getParameter("pendapatanFilm"));
		int nominasiFilm = Integer.parseInt(request.getParameter("nominasiFilm"));
		
		
		FilmModel filmModel = new FilmModel();
		
	
		filmModel.setKodeFilm(kodeFilm);
		filmModel.setNamaFilm(namaFilm);
		filmModel.setGenreFilm(genreFilm);
		filmModel.setKodeArtis(kodeArtis);
		filmModel.setProduserFilm(produserFilm);
		filmModel.setPendapatanFilm(pendapatanFilm);
		filmModel.setNominasiFilm(nominasiFilm);
		
		
		
		this.filmService.create(filmModel);
		this.kodeRead(model);
		
		String create = "/film/home";
		return create;
	}
	private void kodeRead(Model model) {
		List<FilmModel> filmModelList  = new ArrayList<FilmModel>();
		filmModelList = this.filmService.read();
		model.addAttribute("filmModelList", filmModelList);
	}
	
	@RequestMapping("/list")
	public String goList(Model model) {
		this.kodeRead(model);
		
		String list = "/film/list";
		return list;
	}
	
	
	public void artisRead(Model model) {
		List<ArtisModel> artisModelList = new ArrayList<ArtisModel>();
		artisModelList = artisService.read();
		model.addAttribute("artisModelList", artisModelList);
	}
	
	@RequestMapping("/detail")
	public String detail(HttpServletRequest request, Model model) {
		String kodeFilm = request.getParameter("kodeFilm");	
		FilmModel filmModel = new FilmModel();
		
		filmModel = this.filmService.search(kodeFilm);
		model.addAttribute("filmModel", filmModel);
		
		String detail = "/film/detail";
		return detail;
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request,Model model) {
		String kodeFilm = request.getParameter("kodeFilm");
		
		FilmModel filmModel = new FilmModel();
		
		filmModel = this.filmService.search(kodeFilm);
		model.addAttribute("filmModel", filmModel);
		
		this.doListArtis(model);
		return "/film/ubah";
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request, Model model) {
		String kodeFilm = request.getParameter("kodeFilm");
		String namaFilm = request.getParameter("namaFilm");
		String genreFilm = request.getParameter("genreFilm");
		String kodeArtis  = request.getParameter("artis");
		String produserFilm = request.getParameter("produserFilm");
		int pendapatanFilm = Integer.parseInt(request.getParameter("pendapatanFilm"));
		int nominasiFilm = Integer.parseInt(request.getParameter("nominasiFilm"));
	
		
		//instance
		FilmModel filmModel= new FilmModel();
		filmModel.setKodeFilm(kodeFilm);
		filmModel.setNamaFilm(namaFilm);
		filmModel.setKodeFilm(kodeFilm);
		filmModel.setGenreFilm(genreFilm);
		filmModel.setKodeArtis(kodeArtis);
		filmModel.setProduserFilm(produserFilm);
		filmModel.setPendapatanFilm(pendapatanFilm);
		filmModel.setNominasiFilm(nominasiFilm);
		
		
		this.filmService.update(filmModel);
		this.artisRead(model);
		String page="/film/home";
		return page;
		
	}
	@RequestMapping("/bersih")
	public String doBersih(HttpServletRequest request,Model model) {
		String kodeFilm = request.getParameter("kodeFilm");
		
		FilmModel filmModel = new FilmModel();
		
		filmModel = this.filmService.search(kodeFilm);
		
		model.addAttribute("filmModel", filmModel);
		
		return "/film/hapus";
	}
	
	@RequestMapping("/delete")
	public String doDelete(HttpServletRequest request) {
		String kodeFilm = request.getParameter("kodeFilm");
		FilmModel filmModel = new FilmModel();
		
		filmModel = this.filmService.search(kodeFilm);
		this.filmService.delete(filmModel);
		
		String page="/film/home";
		return page;
	}
	
	@RequestMapping(value="/search")
	public String cariNamaFilm(HttpServletRequest request, Model model) {
				
		String namaFilm = request.getParameter("namaFilm");
				
		List<FilmModel> filmModelList = new ArrayList<FilmModel>();
		filmModelList = filmService.searchName(namaFilm);
		model.addAttribute("filmModelList", filmModelList);
		String html = "/film/search";
		return html;
		
	}
	
}
