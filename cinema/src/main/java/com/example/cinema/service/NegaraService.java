package com.example.cinema.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.cinema.model.NegaraModel;
import com.example.cinema.repository.NegaraRepository;

@Service
@Transactional
public class NegaraService {

	@Autowired
	private NegaraRepository negaraRepository;
	
	// method untuk service create
		public void create(NegaraModel negaraModel) {
			this.negaraRepository.save(negaraModel);
		}
	// method list/untuk menampilkan semua data fakultas
		public List<NegaraModel> read(){
			return this.negaraRepository.findAll(); 
		}
	
	// method search berdasarkan kodeFakultas
		public NegaraModel searchKodeNegara(String kodeNegara){
			return this.negaraRepository.searchKodeNegara(kodeNegara);
		}
		
	// method untuk service update
		public void update(NegaraModel negaraModel) {
			this.negaraRepository.save(negaraModel);
		}
		
	// method untuk service delete
		public void delete(NegaraModel negaraModel) {
		this.negaraRepository.delete(negaraModel);
		}

	// method untuk search nama
		public List<NegaraModel> searchNamaNegara(String namaNegara){
			return this.negaraRepository.searchNamaNegara(namaNegara);
		}
	
}
