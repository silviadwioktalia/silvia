package com.example.cinema.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="T_FILM")
public class FilmModel {

	@Id
	@Column(name = "KODE_FILM")
	private String kodeFilm;
	
	@Column(name = "NAMA_FILM")
	private String namaFilm;
	
	@Column(name = "GENRE_FILM")
	private String genreFilm;
	
	@Column(name="KODE_ARTIS")
	private String kodeArtis;
	
	@Column(name = "PRODUSER")
	private String produserFilm;
	
	@Column(name = "PENDAPATAN")
	private int pendapatanFilm;
	


	

	@ManyToOne
	@JoinColumn(name="KODE_ARTIS", nullable=true, insertable=false, updatable=false)
	private ArtisModel artisModel;

	@Column(name = "NOMINASI")
	private int nominasiFilm;
	
	public String getKodeFilm() {
		return kodeFilm;
	}

	public void setKodeFilm(String kodeFilm) {
		this.kodeFilm = kodeFilm;
	}

	public String getNamaFilm() {
		return namaFilm;
	}

	public void setNamaFilm(String namaFilm) {
		this.namaFilm = namaFilm;
	}

	public String getGenreFilm() {
		return genreFilm;
	}

	public void setGenreFilm(String genreFilm) {
		this.genreFilm = genreFilm;
	}

	public String getProduserFilm() {
		return produserFilm;
	}

	public void setProduserFilm(String produserFilm) {
		this.produserFilm = produserFilm;
	}

	public int getPendapatanFilm() {
		return pendapatanFilm;
	}

	public void setPendapatanFilm(int pendapatanFilm) {
		this.pendapatanFilm = pendapatanFilm;
	}

	public int getNominasiFilm() {
		return nominasiFilm;
	}

	public void setNominasiFilm(int nominasiFilm) {
		this.nominasiFilm = nominasiFilm;
	}

	public String getKodeArtis() {
		return kodeArtis;
	}

	public void setKodeArtis(String kodeArtis) {
		this.kodeArtis = kodeArtis;
	}

	public ArtisModel getArtisModel() {
		return artisModel;
	}

	public void setArtisModel(ArtisModel artisModel) {
		this.artisModel = artisModel;
	}
}
